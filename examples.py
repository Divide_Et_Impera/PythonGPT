from PythonGPT import Idea


class ThirdPartyLibraries(Idea):
	name = "name of the third party library"

class LanguageType(Idea):
	is_interpreted = "true if the language is interpreted"

class Python(Idea):
	__name__ = "programming language"
	__about__ = "a programming language called python"
	most_common_implementation = "name of the most common implementation of python"
	language_type = LanguageType
	builtin_functions = ["list of built-in functions"]
	third_party_libraries = [ThirdPartyLibraries]

if __name__ == "__main__":
	python = Python.think()
	print("Python has these builtin functions:",*python.builtin_functions)
	print(python.most_common_implementation,"is the most common implementation of python")
	print("Is python intepreted?",python.language_type.is_interpreted)

