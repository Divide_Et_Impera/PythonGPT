#!/usr/bin/python3

from setuptools import setup
import os

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()
setup(name="PythonGPT",
	version="1.0",
	description="Simple library for integrating ChatGPT into your project",
	author="Divide Et Impera",
	url="https://gitlab.com/Divide_Et_Impera/PythonGPT",
	license = "GPL v3",
	packages=["PythonGPT"],
	long_description=read('README.md'),
	long_description_content_type="text/markdown",
	install_requires=["openai"],
	keywords=["python","chatgpt","openai"]
)
