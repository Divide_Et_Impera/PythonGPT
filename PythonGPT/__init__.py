import openai
import os
import re
import inspect
import json
import time

openai.api_key = os.getenv("CHATGPT_API")

class Idea:
	"""
	You must include the following attributes to your Idea subclass
	
	__name__: what json object you want ChatGPT to return
	__about__: a description of the json object
	
	The json object must not be too big or ChatGPT will show error
	"""
	def __init__(self,_dict):
		self.__dict__.update(_dict)

	@classmethod
	def create(cls, _dict):
		return cls({
			k:cls.__dict__[k](v) if isinstance(v,dict)
			else [cls.__dict__[k][0](obj)
				if isinstance(obj,Idea)
				else obj for obj in v
			] if isinstance(v,list)
			else v
			for k, v in _dict.items()
		})

	@classmethod
	def think(cls,model="gpt-3.5-turbo"):
		_dict = PythonGPT(model=model).get_json(cls.call())
		return cls.create(_dict)

	@classmethod
	def attributes(cls):
		def to_dict(obj): # idk how to name this shit
			if inspect.isclass(obj):
				return obj.attributes()
			elif isinstance(obj, list):
				return [o.attributes() if isinstance(o,Idea) else o 
					for o in obj
				]
			else:
				return obj

		return {k:to_dict(v) for k,v in cls.__dict__.items() 
			if not k.startswith("__") and 
			not inspect.isfunction(v)
		}


	@classmethod
	def call(cls):
		return f"Write me a {cls.__name__} about {cls.__about__} in json format using the following schema\n{cls.attributes()}"


class PythonGPT:
	def __init__(self, model,
		sys_msg="You are a json generator, you do not talk and you write json only, "\
			"remember to meet the user's requirements"
	):
		self.messages = [{"role": "system", "content": sys_msg}]	
		self.model = model
	
	def _call(self):
		while True:
			try:return openai.ChatCompletion.create(
					model=self.model,
					messages=self.messages
			).choices[0].message.content
			except openai.error.RateLimitError:
				print("Rate limit reached, waiting 20 seconds...")
				time.sleep(20)
	
	
	def call(self, prompt, role="user"):
	
		self.messages.append({"role": role, "content": prompt})
		response_text = self._call()
		self.messages.append({"role": "assistant", "content": response_text})
		#print(response_text)
		return response_text

	def get_json(self, prompt=None):
		def fix_json(e, stored_json):
			return self.get_json(
					f"The json is incorrect and returned {e} when parsing it"\
					f"please fix it so that it can be parsed correctly {stored_json}"
				)
		stack = 0
		stored_json = ""
		
		for c in self.call(prompt):
			if c == "{": stack += 1
			if stack > 0: stored_json += c
			if c == "}": stack -= 1
		try:
			return json.loads(stored_json)
		except Exception as e:
			return fix_json(e, stored_json)

